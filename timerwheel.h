#ifndef TIMER_WHEEL_H
#define TIMER_WHEEL_H

#include <time.h>
#include <netinet/in.h>
#include <stdio.h>
#include <malloc.h>

#define BUFFER_SIZE 64

typedef struct twTimer TwTimer;

typedef struct clientData
{
	sockaddr_in address;
	int sockfd ;
	char buff[BUFFER_SIZE] ;
	TwTimer *timer ;
}ClientData ;

struct twTimer
{
	int rotation ;// 记录轮询多少次后生效
	int timeSlot ;// 记录定时器属于哪个槽
	void (*cbFunc)(ClientData*) ;
	ClientData *userData ;
	TwTimer *prev ;
	TwTimer *next ;
};

class TimerWheel
{
	public:
		TimerWheel():curSlot(0)
		{
			for(int i = 0 ;i<N ;i++)
			{
				slots[i]=NULL ;
			}
		}

		~TimerWheel()
		{
			TwTimer *tmp = NULL ;
			for(int i = 0 ;i< N ;i++)
			{
				tmp = slots[i] ;
				while(tmp)
				{
					slots[i] = tmp->next ;
					free(tmp) ;
					tmp = slots[i] ;
				}
			}
		}

		TwTimer * addTimer(int timeout)
		{
			if(timeout < 0)
			{
				return NULL ;
			}

			int ticks ;
			if(timeout <SI)
			{
				ticks = 1 ;
			}
			else
			{
				ticks = timeout/SI ;
			}

			int rotation = ticks/N ; //多少圈被触发
			int ts = (curSlot+(ticks%N))%N ; // 具体的槽

			TwTimer *timer = (TwTimer*)malloc(sizeof(TwTimer)) ;
			timer->rotation = rotation ;
			timer->timeSlot = ts ;

			if(!slots[ts])
			{
				slots[ts] = timer ;
			}
			else
			{
				timer->next = slots[ts] ;
				slots[ts]->prev = timer ;
				slots[ts] = timer ;
				timer->prev = NULL ;
			}

			return timer ;
		}
		
		void delTimer(TwTimer * timer)
		{
			if(!timer)
			{
				return ;
			}
			
			int ts = timer->timeSlot ;
			if(timer == slots[ts])
			{
				slots[ts]=slots[ts]->next ;
				if(slots[ts])
				{
					slots[ts]->prev = NULL ;
				}

				free(timer) ;
			}
			else
			{
				timer->prev->next = timer->next ;
				if(timer->next)
				{
					timer->next->prev = timer->prev ;
				}
				free(timer) ;

			}
		}
		
		void tick()
		{
			TwTimer *pCurSlot = slots[curSlot] ;
			printf("currnet slot is %d\n",curSlot) ;
			while(pCurSlot)
			{
				printf("tick the timer once\n") ;
				
				if(pCurSlot->rotation > 0)
				{
					pCurSlot->rotation -- ;
					pCurSlot = pCurSlot->next ;
				}
				else
				{
				
					if(pCurSlot->cbFunc)
						pCurSlot->cbFunc(pCurSlot->userData) ;
					if(pCurSlot == slots[curSlot])
					{
						printf("delete header in curSlot\n") ;
						slots[curSlot] = pCurSlot->next ;
						free(pCurSlot) ;
						
						if(slots[curSlot])
						{
							slots[curSlot]->prev = NULL ;
						}

						pCurSlot = slots[curSlot] ;

					}
					else
					{
						pCurSlot->prev->next = pCurSlot->next ;
						if(pCurSlot->next)
						{
							pCurSlot->next->prev = pCurSlot->prev ;
						}
						TwTimer *pNext = pCurSlot->next ;
						free(pCurSlot) ;
						pCurSlot = pNext ;
					} // end if pCurSlot == slots[curSlot]
				}// end if pCurSlot->rotation >0
			}// while(pCurSlot
		curSlot = ++curSlot %N ;
		}

	private :
		// 槽数量
		static const int N = 60 ;
		//转动间隔
		static const int SI = 1 ;
		//时间槽
		TwTimer *slots[N] ;
		int curSlot ;//时间轮的当前槽
} ;


#endif
