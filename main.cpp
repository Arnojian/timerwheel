#include <iostream>
#include <sys/select.h>
#include <unistd.h>

#include "timerwheel.h"

int main()
{
	TimerWheel *pTimerWheel = new TimerWheel() ;

	timeval tv ;
	tv.tv_sec = 1 ;
	tv.tv_usec = 0 ;
	bool flag = true ;
	while(1)
	{
		if(flag)
		{
			pTimerWheel->addTimer(10) ;
			pTimerWheel->addTimer(20);
			pTimerWheel->addTimer(40);
			
			TwTimer *ptmp = pTimerWheel->addTimer(30);
			pTimerWheel->delTimer(ptmp);	
			flag = false ;
		}
		tv.tv_sec =1;
		tv.tv_usec = 0;
		int ret = select(0,NULL,NULL,NULL,&tv) ;
		if(ret < 0)
		{
			std::cout<<"error"<<std::endl;
			return -1 ;
		}
		else if(ret ==0)
		{
			printf("tick sec:%d,usec:%d\n",tv.tv_sec,tv.tv_usec);
			pTimerWheel->tick();
		}
	}
	return 0 ;
}
